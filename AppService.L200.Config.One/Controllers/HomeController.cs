﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppService.L200.Config.One.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult ConfigOne()
        {
            string websiteOne = System.Environment.GetEnvironmentVariable("WEBSITE_ONE");
            string websiteTwo = System.Environment.GetEnvironmentVariable("WEBSITE_TWO");

            if(string.IsNullOrEmpty(websiteOne) || string.IsNullOrEmpty(websiteTwo))
            {
                websiteOne = HttpContext.Request.Url.ToString() + "json";
                if (websiteOne.Contains("://1"))
                {
                    websiteTwo = websiteOne.Replace("://1", "://2");
                }
                else
                {
                    websiteTwo = websiteOne;
                    websiteOne = websiteOne.Replace("://2", "://1");
                }
            }
            ViewBag.WebsiteOne = websiteOne;
            ViewBag.WebsiteTwo = websiteTwo;

            return View();
        }

        public ActionResult ConfigOneJson()
        {
            return Json(new { name = "Black", code = "Tiger" }, JsonRequestBehavior.AllowGet);
        }

         
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}